<?php

/**
 * @file
 * Install, update and uninstall functions for the notification module module.
 */

use Drupal\Core\Database\Database;
use Drupal\Core\Url;

/**
 * Implements hook_schema().
 */
function notifications_widget_schema() {
  $schema['notifications'] = [
    'description' => 'Create table used to store data of each site notifications of users',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique ID for this record.',
      ],
      'entity_id' => [
        'description' => 'Entity id of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'entity_uid' => [
        'description' => 'Entity uid of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'action' => [
        'description' => 'Actions type Create, Update, Delete',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ],
      'bundle' => [
        'description' => 'Name of the bundle',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'uid' => [
        'description' => 'uid of actioned user',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'user_name' => [
        'description' => 'Name of the user',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'message' => [
        'description' => 'Notification message based on configuration',
        'type' => 'text',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'status' => [
        'description' => 'Status of the notification',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'description' => "Timestamp for the record insert.",
      ],
    ],
    'indexes' => [
      'notification_uid' => ['uid'],
      'notification_entity' => ['entity_id'],
      'notification_created_timestamp' => ['created'],
    ],
    'primary key' => ['id'],
  ];

  $schema['notifications_actions'] = [
    'description' => 'Create table used to store data of each action on notifications by the users',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique ID for this record.',
      ],
      'notification_id' => [
        'description' => 'Notificaiton id of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'uid' => [
        'description' => 'Entity uid of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Status of the notification 1=read, 2=>delete',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'description' => "Timestamp for the record insert.",
      ],
    ],
    'indexes' => [
      'notification_id' => ['notification_id'],
      'uid' => ['uid'],
      'notification_update_timestamp' => ['created'],
    ],
    'primary key' => ['id'],
  ];
  $schema['notifications_clear_all'] = [
    'description' => 'Create table used to store data for clearall status',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique ID for this record.',
      ],
      'notification_id' => [
        'description' => 'Last Notificaiton id of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'uid' => [
        'description' => 'logged-in uid of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'description' => "Timestamp for the record insert.",
      ],
    ],
    'indexes' => [
      'uid' => ['uid'],
      'notification_update_timestamp' => ['created'],
    ],
    'primary key' => ['id'],
  ];
  return $schema;
}

/**
 * Implements hook_install().
 */
function notifications_widget_install() {
  // Inform the user about save the widget configuration.
  \Drupal::messenger()->addMessage(t('Notification widget will work well once you saved the configuration from <a href=":user_settings_url">Notification Widget Settings</a>.', [
    ':user_settings_url' => Url::fromRoute('notifications_widget.notifications_widget_settings')->toString(),
  ]), 'warning');
}

/**
 * Update existing messsage value (adding class in message).
 */
function notifications_widget_update_8101() {
  $connection = Drupal::database();
  $query = $connection->select('notifications', 'n');
  $query->fields('n', [
    'id',
    'message',
    'status',
  ]);
  $query->condition('n.message', '%%class = "noti-store-msg"%%', 'NOT LIKE');
  $query->orderBy('n.created', 'DESC');
  $res = $query->execute();

  while ($notification = $res->fetchObject()) {
    $keys = [
      'id' => $notification->id,
    ];

    $replacedStr = 'class="noti-store-msg" href="javascript:;"';

    $fields = [
      'message' => str_replace('href="javascript:;"', $replacedStr, $notification->message),
    ];
    $connection->merge('notifications')
      ->key($keys)
      ->fields($fields)
      ->execute();
  }
}

/**
 * Adding column entity user id.
 */
function notifications_widget_update_8102() {
  $connection = \Drupal::database();
  $columnExists = $connection->schema()->fieldExists('notifications', 'entity_uid');
  if ($columnExists == '') {
    $spec = [
      'type' => 'int',
      'description' => 'Entity uid of action performed',
      'AFTER' => 'entity_id',
    ];
    $schema = Database::getConnection()->schema();
    $schema->addField('notifications', 'entity_uid', $spec);
  }
}

/**
 * Set the module weight to maximum.
 */
function notifications_widget_update_8103() {
  module_set_weight('notifications_widget', 100);
}

/**
 * Create new schema for notifications actions.
 */
function notifications_widget_update_8104() {
  $spec = [
    'description' => 'Create table used to store data of each action on notifications by the users',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique ID for this record.',
      ],
      'notification_id' => [
        'description' => 'Notificaiton id of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'uid' => [
        'description' => 'Entity uid of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'status' => [
        'description' => 'Status of the notification 1=read, 2=>delete',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'description' => "Timestamp for the record insert.",
      ],
    ],
    'indexes' => [
      'notification_id' => ['notification_id'],
      'uid' => ['uid'],
      'notification_update_timestamp' => ['created'],
    ],
    'primary key' => ['id'],
  ];
  $clearAllSpec = [
    'description' => 'Create table used to store data for clearall status',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'Unique ID for this record.',
      ],
      'notification_id' => [
        'description' => 'Last Notificaiton id of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'uid' => [
        'description' => 'logged-in uid of action performed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'description' => "Timestamp for the record insert.",
      ],
    ],
    'indexes' => [
      'uid' => ['uid'],
      'notification_update_timestamp' => ['created'],
    ],
    'primary key' => ['id'],
  ];
  $schema = Database::getConnection()->schema();
  $schema->createTable('notifications_actions', $spec);
  $schema->createTable('notifications_clear_all', $clearAllSpec);
}

/**
 * Make notifications message field in DB bigger, so it won't throw errors.
 */
function notifications_widget_update_8105(&$sandbox) {
  $connection = \Drupal::database();

  if (!$connection->schema()->fieldExists('notifications', 'message')) {
    return;
  }

  // Make field larger because it throws errors when size above 255.
  $connection->schema()->changeField('notifications', 'message', 'message', [
    'description' => 'Notification message based on configuration',
    'type' => 'text',
    'size' => 'big',
    'not null' => TRUE,
  ]);
}
