<?php

namespace Drupal\notifications_widget\Services;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Utility\Token;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service handler for Notification Logger.
 */
class NotificationsWidgetService implements NotificationsWidgetServiceInterface {

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Symfony\Component\HttpFoundation\Request definition.
   *
   * @var Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Logger Factory Interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Utility\Token definition.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    AccountProxy $current_user,
    Connection $database,
    RequestStack $request,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $loggerFactory,
    Token $token,
    TimeInterface $time,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->currentUser = $current_user;
    $this->database = $database;
    $this->request = $request;
    $this->configFactory = $config_factory;
    $this->loggerFactory = $loggerFactory->get('activity_tracking');
    $this->token = $token;
    $this->time = $time;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function logNotification(
    array $message,
    string $userAction,
    object $entity,
    int $recipient_uid = NULL,
    int $operator_uid = NULL
  ): void {
    if (!$recipient_uid) {
      if ($entity->bundle() === 'user') {
        $recipient_uid = $entity->id();
      }
      elseif (method_exists($entity, 'getOwner')) {
        $recipient_uid = $entity->getOwner()->id();
      }
      else {
        $recipient_uid = 1;
      }
    }

    if (!$operator_uid) {
      $operator_uid = $this->currentUser->id();
      $operator_username = $this->currentUser->getDisplayName();
    }
    else {
      $operator_username = $this->entityTypeManager->getStorage('user')
        ->load($operator_uid)->getDisplayName();
    }

    // Fetch the excluded entities to notifications.
    $notificationConfig = $this->configFactory->get('notifications_widget.settings');
    $excludeList = $notificationConfig->get('excluded_entities');
    $excludeBundles = explode(',', (string) $excludeList);

    $entityUrl = $message['content_link'];
    if ($entityUrl === '[entity:url]' && $entity->hasLinkTemplate('canonical')) {
      $entityUrl = $entity->toUrl()->toString();
    }

    // Validate for bundle exclusion.
    if (!in_array($entity->bundle(), $excludeBundles)) {

      // Prepare notification item link.
      $messageItems = '<a class="noti-store-msg" href="javascript:;" data-link="' . $entityUrl . '">' . $message['content'] . '</a>';

      $messageItems = $this->token->replace(
        $messageItems, [
          'user' => $this->currentUser,
          'node' => $entity,
          'term' => $entity,
          'comment' => $entity,
        ]
      );

      $keys = [
        'id' => NULL,
      ];

      $fields = [
        'entity_id'  => $message['id'],
        'entity_uid' => $recipient_uid,
        'action'     => $userAction,
        'bundle'     => $message['bundle'],
        'uid'        => $operator_uid,
        'user_name'  => $operator_username,
        'message'    => $messageItems,
        'status'     => 0,
        'created'    => $this->time->getRequestTime(),
      ];

      try {
        $this->database->merge('notifications')
          ->key($keys)
          ->fields($fields)
          ->execute();
      }
      catch (Exception $e) {
        // Exception handling if something else gets thrown.
        $this->loggerFactory->error($e->getMessage());
      }
    }
  }

}
